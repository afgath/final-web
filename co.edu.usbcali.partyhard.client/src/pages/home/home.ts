import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SongsProvider } from '../../providers/songs/songs';
import { config } from '../../providers/config';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  songs: any[];
  config: any = config;
  constructor(public navCtrl: NavController,
    private songsProvider: SongsProvider) {

    this.songsProvider.getAll().subscribe(
      data => this.songs = data.json(),
      error => console.log(error),
      () => console.log(this.songs)
    );

  }

  addSong(obj) {
   // let peticion: any = song;
    console.log(obj.id);
    this.songsProvider.peticionAnadir(obj.id).subscribe(
      data => {
        console.log("hola"+data);          
      },
      err => console.log(err),
      () => console.log('peticion de borrar cancion')
    );
  }




 
}
