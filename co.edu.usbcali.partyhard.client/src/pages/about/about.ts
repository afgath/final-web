import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Player } from '../../models/Player';
import { SongsProvider } from '../../providers/songs/songs';
import { config } from '../../providers/config';
import { PlaylistProvider } from '../../providers/playlist/playlist';
import { AlertController } from 'ionic-angular';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
 
  player: Player;
  playlist: any[];
  peticion: any[];
  playing: any = false;
  config: any = config;

  constructor(public navCtrl: NavController, 
    private songsProvider: SongsProvider, 
    private playlistProvider: PlaylistProvider, private alertCtrl: AlertController) {

    this.playlistProvider.getAll().subscribe(
      data => this.playlist = data.json(),
      error => console.log(error),
      () => console.log(this.playlist)
    );
    
    this.refresh();
    //this.player = new Player(this.playlist[1].title, this.playlist[1].artist, "Knights of Cydonia Single", this.playlist[1].cover, this.playlist[1].src,null);
  }


  removeSong(obj) {  
      console.log(obj.id);
      this.playlistProvider.peticionBorrar(obj.id).subscribe(
        data => {
          console.log("hola"+data);          
        },
        err => console.log(err),
        () => console.log('peticion de borrar cancion')
      );      
  }

  /*refresh(){
    setInterval(() => {         
      this.playlistProvider.getAll().subscribe(
        data => {this.playlist = data.json()},
        error => console.log(error),
        () => console.log(this.playlist)
      );
    }, 10000);
  }*/

  refresh(){
    setInterval(() => {         
      this.playlistProvider.getAll().subscribe(
        data => {this.playlist = data.json()},
        error => console.log(error),
        () => this.presentConfirm(),        
      );
    }, 150000);
  }

  presentConfirm() {      
    this.playlistProvider.getPeticion().subscribe(
      data => {
        this.peticion=data.json();
        let alert = this.alertCtrl.create({
          title: 'Deseas realizar esta acción?',
          message: (this.peticion['0']['tipo_peticion']=="N")?'Vas a votar por cancion #'+this.peticion['0']['Song_id']+' a que se elimine':'Vas a votar por cancion #'+this.peticion['0']['Song_id']+' a que se agregue',
          buttons: [
            {
              text: 'Cancelar',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
                this.playlistProvider.getRespuestaMalaPeticion(this.peticion['0']['idpeticion']).subscribe(
                  data => {console.log(data.json())},
                  error => console.log(error),                  
                );                
              }
            },
            {
              text: 'confirmar',
              handler: () => {                
                console.log('confirmado');
                //var voto: any = "S"
                this.playlistProvider.getRespuestaBuenaPeticion(this.peticion['0']['idpeticion']).subscribe(
                  data => {console.log(data.json())},
                  error => console.log(error));  
              }
            }
          ]
        });
        alert.present();
        console.log("peticion id cancion:"+this.peticion['0']['Song_id']);          
      },
      err => console.log(err),
      () => console.log('peticion a votar')
    ); 

 
  }


  /*presentConfirm() {      
    this.playlistProvider.getPeticion().subscribe(
      data => {
        this.peticion=data.json();
        console.log("peticion id cancion:"+this.peticion['0']['Song_id']);          
      },
      err => console.log(err),
      () => console.log('peticion a votar')
    ); 

    let alert = this.alertCtrl.create({
      title: 'Deseas realizar esta acción?',
      message: 'hola',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'confirmar',
          handler: () => {
            console.log('confirmado');
            var voto: any = "S"
          }
        }
      ]
    });
    alert.present();
  }*/

}
