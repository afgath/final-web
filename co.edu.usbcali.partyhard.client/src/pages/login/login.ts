import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UsersProvider } from '../../providers/users/users';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public name: String;
  private user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private up: UsersProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  party() {
    
    this.up.newUser(this.name).subscribe(
      data => {
        console.log(data);
        this.user = data.json();
        //console.log(this.user);
        this.storage.set('user',this.user['0']['count(*)']);
        //console.log("localstorage:"+this.user['0']['count(*)']);
        this.navCtrl.push(TabsPage);
      },
      err => console.log(err),
      () => console.log('Se unió a la fiesta')
    );
  }

  redirect() {
    
  }

}
