import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { config } from '../config';

/*
  Generated class for the PlaylistProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlaylistProvider {

  constructor(public http: Http) {
    console.log('Hello PlaylistProvider Provider');
  }

  getAll() {
    return this.http.get(config.service_location + "Playlist/get");
  }

  getPeticion() {
    return this.http.get(config.service_location + "Playlist/getPeticion");
  }

  getRespuestaMalaPeticion(idPeticion) {
    return this.http.get(config.service_location + "Playlist/getRespuestaMalaPeticion/"+idPeticion);
  }
  
  getRespuestaBuenaPeticion(idPeticion) {
    return this.http.get(config.service_location + "Playlist/getRespuestaBuenaPeticion/"+idPeticion);
  }


  peticionBorrar(id){
    return this.http.get(config.service_location + "Peticion/newPetitionBorrar/"+id);
  }

}
