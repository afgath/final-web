import { Injectable } from '@angular/core';
import { Song } from '../../models/Song';
import { Http } from '@angular/http';
import { config } from '../config';

/*
  Generated class for the SongsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SongsProvider {

  songs: Song[]; 

  constructor(public http: Http) {
    
  }

  get(id) {
    return this.http.get(config.service_location + "Songs/get/"+id);
  }

  getAll() {
    return this.http.get(config.service_location + "Songs/get");
  }

  peticionAnadir(id) {
    return this.http.get(config.service_location + "Peticion/newPetition/"+id);
  }
  
}
