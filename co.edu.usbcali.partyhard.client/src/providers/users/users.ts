import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { config } from '../config';

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersProvider {

  constructor(public http: Http) {
  }

  newUser(nombre) {
    return this.http.get(config.service_location + "User/newUser/" + nombre);
  }

}
