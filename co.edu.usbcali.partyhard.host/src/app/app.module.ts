import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Http, HttpModule } from '@angular/http';

import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { EscapeHtmlPipe } from '../pipes/keep-html/keep-html';
import { SongsProvider } from '../providers/songs/songs';
import { PlaylistProvider } from '../providers/playlist/playlist';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    EscapeHtmlPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SongsProvider,
    PlaylistProvider
  ]
})
export class AppModule {}
