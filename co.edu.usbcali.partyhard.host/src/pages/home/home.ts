import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Player } from '../../models/Player';
import { SongsProvider } from '../../providers/songs/songs';
import { config } from '../../providers/config';
import { PlaylistProvider } from '../../providers/playlist/playlist';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  player: Player;
  playlist: any[];
  playing: any = false;
  config: any = config;

  constructor(public navCtrl: NavController, 
    private songsProvider: SongsProvider, 
    private playlistProvider: PlaylistProvider) {

    this.playlistProvider.getAll().subscribe(
      data => this.playlist = data.json(),
      error => console.log(error),
      () => console.log(this.playlist)
    );
    
    this.refresh();
    //this.player = new Player(this.playlist[1].title, this.playlist[1].artist, "Knights of Cydonia Single", this.playlist[1].cover, this.playlist[1].src,null);
  }

  playSong(song){
    let player: any = document.querySelector("#player audio");
    let playerTitle: any = document.querySelector("#player .title");
    let playerAA: any = document.querySelector("#player .artistAlbum");
    playerTitle = song.title;
    playerAA = song.artist+"-"+song.album;
    player.src = config.service_location+song.src;
    player.play();

    let cover:any = document.querySelector("#player img");
    cover.src = config.service_location +song.cover;
  }

  playpause(e){
    let audioPlayer = e.target.parentElement.parentElement.querySelector("audio");
    if(audioPlayer.paused){ 
      audioPlayer.play();
      this.playing = true;
    }else{ 
      audioPlayer.pause();
      this.playing = false;
    }
  }
  bacward(){

  }
  forward(){

  }

  refresh(){
    setInterval(() => {         
      this.playlistProvider.getAll().subscribe(
        data => {this.playlist = data.json()},
        error => console.log(error),
        () => console.log(this.playlist)
      );
    }, 1000);
  }

}
