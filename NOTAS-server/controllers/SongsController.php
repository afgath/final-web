<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MateriaController
 *
 * @author brahian-Andres
 */
 //require './DAOs/UsuarioDAO.php';
require './conexion/Conexion.php';


class SongsController 
{
    public function index()
    {
        echo "Songs Controller";
    }
    
    public function get($id = null){
     
        $r = Song::get($id);
        print json_encode($r, JSON_UNESCAPED_SLASHES);
        
    }
    
    public static function peticionAnadir($id = null){
        $r = Song::peticionAnadir($id);
        print json_encode($r['0'], JSON_UNESCAPED_SLASHES);
      }

}
