<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LibretaController
 *
 * @author brahian
 */
//require './conexion/Conexion.php';
//require './models/Playlist.php';

class PlaylistController 
{
    public function index()
    {
        echo "Playlist Controller";
    }
    
    public function get(){
        $playlist = Playlist::get();
        $songs = [];
        foreach ($playlist as $key=>$song) {
            $songs[$key]["song"] = Song::get($song["id_song"])[0];
            $songs[$key]["points"] = $song["points"];
        }
        print json_encode($songs);
    }
    
    public function peticionBorrar($id = null){
        $r=Song::peticionBorrar($id);
        print json_encode($r, JSON_UNESCAPED_SLASHES);
    }

    public function getRespuestaMalaPeticion($id = null){
        $r=Song::peticionMalaAnadir($id);
        print json_encode($r, JSON_UNESCAPED_SLASHES);
    }

    public function getRespuestaBuenaPeticion($id = null){
        $r=Song::getRespuestaBuenaPeticion($id);
        print json_encode($r, JSON_UNESCAPED_SLASHES);
    }
    

    public function getPeticion(){
        $r=Song::getPeticion();
        print json_encode($r, JSON_UNESCAPED_SLASHES);
    }

}
