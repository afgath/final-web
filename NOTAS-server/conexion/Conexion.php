<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataBase
 *
 * @author brahian
 */
require_once 'config.php';

class Conexion {
    
    protected $connection;
    protected $db;
    
    public function connect() {        
        $this->connection = mysqli_connect(HOST, USER, PASS, DBNAME) or die("Error conectando a la BD");
        mysqli_set_charset('utf8',$this->connection);
    }
    
    public function disconnect() {
        mysqli_close($this->connection);

    }

    public function testdb() {
        $tabla = "TU_TABLA";
        $query = mysql_query("SELECT count(*) from $tabla", $this->connection);
        if ($query == 0) echo "Sentencia incorrecta llamado a tabla: $tabla.";
        else {
            $nregistrostotal = mysql_result($query, 0, 0);
            echo "Hay $nregistrostotal registros en la tabla: $tabla.";
            mysql_free_result($query);
        }
    }
    

}
