<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Penelope
 *
 * @author brahian
 */
class Penelope {
    
    public static function arrayToJSON($array) {
        array_walk_recursive($array, 'Penelope::encode_items');
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
    
    private static function encode_items(&$item) {
        $item = utf8_encode($item);
    }
    
    public static function printJSON($array) {
        print(Penelope::arrayToJSON($array));
    }
    
    
}
