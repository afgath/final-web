<?php

spl_autoload_register(function($class){

    if(file_exists("controllers/".$class.".php")){
        require "controllers/".$class.".php";
        return 0;
    }
    
    if(file_exists("models/".$class.".php")){
        require "models/".$class.".php";
        return 0;
    }
    
    if(file_exists("libs/".$class.".php")){
        require "libs/".$class.".php";
        return 0;
    }
});
