<?php

class Song {
      private $title;
      private $artist;
      private $album;
      private $src;
      private $cover;
      private $pos;
      
      function __construct($title, $artist, $album, $src, $cover, $pos) {
          $this->title = $title;
          $this->artist = $artist;
          $this->album = $album;
          $this->src = $src;
          $this->cover = $cover;
          $this->pos = $pos;
      }
      
      function getTitle() {
          return $this->title;
      }
  
      function getArtist() {
          return $this->artist;
      }
  
      function getAlbum() {
          return $this->album;
      }
  
      function getSrc() {
          return $this->src;
      }
  
      function getCover() {
          return $this->cover;
      }
  
      function getPos() {
          return $this->pos;
      }
  
      function setTitle($title) {
          $this->title = $title;
      }
  
      function setArtist($artist) {
          $this->artist = $artist;
      }
  
      function setAlbum($album) {
          $this->album = $album;
      }
  
      function setSrc($src) {
          $this->src = $src;
      }
  
      function setCover($cover) {
          $this->cover = $cover;
      }
  
      function setPos($pos) {
          $this->pos = $pos;
      }
      
      public static function get($id=null){
          $db = new MySQLiManager('localhost','root','','partyhard');
          if(is_null($id)){
              return $db->select("*", "song");
          }else{
              return $db->select("*", "song", "id = $id");
          }
      }

      public static function getPeticion(){
        $db = new MySQLiManager('localhost','root','','partyhard');
        return $db->select("*", "peticion","idpeticion = (select idpeticion from peticion order by idpeticion desc limit 1)");
      }
    
    public static function peticionMalaAnadir($id=null){
      $db = new MySQLiManager('localhost','root','','partyhard');
      if(!is_null($id)) {
        $array = array(   
          "voto"=>'N',                       
          "idpeticion"=>$id
        );
        $db->insert("votos",$array,'',false);//Llamar al insert
        return "Peticion realizada"; //Devolver el id del usuario
      }else{
          return "id nulo";
      }  
    }

    public static function getRespuestaBuenaPeticion($id=null){
        $db = new MySQLiManager('localhost','root','','partyhard');
        if(!is_null($id)) {
          $array = array(   
            "voto"=>'S',                       
            "idpeticion"=>$id
          );
          $db->insert("votos",$array,'',false);//Llamar al insert
          return "Peticion realizada"; //Devolver el id del usuario
        }else{
            return "id nulo";
        }  
      }
    


  }
  
?>
