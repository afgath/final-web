<?php

class Playlist {
      private $id_song;
      private $points;
      
      function __construct($id_song, $points) {
          $this->id_song = $id_song;
          $this->points = $points;
      }
  
      function getId_song() {
          return $this->id_song;
      }
  
      function getPoints() {
          return $this->points;
      }
  
      function setId_song($id_song) {
          $this->id_song = $id_song;
      }
  
      function setPoints($points) {
          $this->points = $points;
      }
  
          
      public static function get(){
          $db = new MySQLiManager('localhost','root','','partyhard');
          return $db->select("*", "playlist");
          
      }
  
      public static function peticionBorrar($id = null){
        $db = new MySQLiManager('localhost','root','','partyhard');
        if(!is_null($id)) {
          $array = array(   
            "voto_aceptado"=>null,             
            "voto_no_aceptado" => 'S',
            "idpeticion"=>$id
          );
          $db->insert("votos",$array,'',false);//Llamar al insert
          return "Peticion realizada"; //Devolver el id del usuario
        }
      }

  }

?>